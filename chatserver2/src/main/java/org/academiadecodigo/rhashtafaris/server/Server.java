package org.academiadecodigo.rhashtafaris.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private Integer portNumber;


    private final List<ClientConnection> clientsList = Collections.synchronizedList(new ArrayList<>());

    private ExecutorService service;

    public Server(Integer portNumber) {
       this.portNumber = portNumber;
       service = Executors.newFixedThreadPool(5);

    }

    public void start() {

        try {

            serverSocket = new ServerSocket(portNumber);

            while (true) {

                clientSocket = serverSocket.accept();
                ClientConnection connection = new ClientConnection(this, clientSocket);
                service.submit(connection);
                addClient(connection);
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private void addClient(ClientConnection connection) {
        synchronized (clientsList) {
            clientsList.add(connection);
        }
    }

    public void broadcast(String message) {

        synchronized (clientsList) {
            for (ClientConnection c : clientsList) {
                c.send(message);
            }
        }
    }

}

