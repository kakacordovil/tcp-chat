package org.academiadecodigo.rhashtafaris.server;

public class ServerLauncher {

    public static final int DEFAULT_PORT = 8080;

    public static void main(String[] args) {

//        int port = args.length == 1 ? Integer.parseInt(args[0]) : DEFAULT_PORT;
        Server server = new Server(DEFAULT_PORT);

        server.start();
    }
}
