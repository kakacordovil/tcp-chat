package org.academiadecodigo.rhashtafaris.server;

import java.io.*;
import java.net.Socket;

public class ClientConnection implements Runnable{

    Socket clientSocket;
    Server server;

    BufferedReader reader;
    PrintWriter writer;

    public ClientConnection(Server server, Socket clientSocket){
        this.clientSocket = clientSocket;
        this.server = server;

    }

    @Override
    public void run() {
        execute();
    }

    public void execute() {

        try {
            reader = openStreams();
//            writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            String welcome = "Welcome to the jungle <server_>!";
            send(welcome);
            System.out.println("Client connected");
            while (!clientSocket.isClosed()){

                String message = listen(reader);
                System.out.println("Client says: " + message);

            }


        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    private BufferedReader openStreams() throws IOException {

        writer = new PrintWriter(clientSocket.getOutputStream(), true);
        return new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    private String listen(BufferedReader reader) throws IOException {

        String message = reader.readLine();
        server.broadcast("Client:" + message);

//        send(message);

        return message;
    }

    public void send(String message) {

            writer.println(message);


    }

}

