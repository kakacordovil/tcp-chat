package org.academiadecodigo.rhashtafaris.client;

import java.io.IOException;

public class ClientLauncher {

    public static final int DEFAULT_PORT = 8080;

    public static void main(String[] args) {

        try {
//            Client client = new Client(args[0], Integer.parseInt(args[1]));
            Client client = new Client("localhost", 8080);

            client.start();

        } catch (IOException e) {

            System.err.println("An error occured: " + e.getMessage());;
        }
    }


}

