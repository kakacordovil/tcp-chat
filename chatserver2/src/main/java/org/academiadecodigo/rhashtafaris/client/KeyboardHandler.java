package org.academiadecodigo.rhashtafaris.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class KeyboardHandler implements Runnable{

    private Socket clientSocket;

    public KeyboardHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(),true);

            while (!clientSocket.isClosed()) {

                String message = reader.readLine();
                writer.println(message);

            }
        } catch (IOException e) {
            System.err.println("Error handling socket connection: " + e.getMessage());
        }

    }
}
