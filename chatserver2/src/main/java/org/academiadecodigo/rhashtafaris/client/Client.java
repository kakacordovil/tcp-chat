package org.academiadecodigo.rhashtafaris.client;

import java.io.*;
import java.net.Socket;

public class Client {

    private Socket clientSocket;

    public Client(String host, int port) throws IOException {
        this.clientSocket = new Socket(host, port);
    }

    public void start() {

//        setupStreams();
//        getInput();
        Thread keyboard = new Thread(new KeyboardHandler(clientSocket));
        keyboard.start();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while (!clientSocket.isClosed()) {
                waitMessage(reader);
            }
        } catch (IOException e) {
            System.err.println("Error handling socket connection: "+ e.getMessage());
        }
    }

    private void waitMessage(BufferedReader reader) throws IOException {

        String message = reader.readLine();

        if (message == null) {
            System.out.println("Connectiong closed from server side.");
            System.exit(1);
        }

        System.out.println(message);
    }

}

